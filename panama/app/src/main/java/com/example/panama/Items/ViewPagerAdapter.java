package com.example.panama.Items;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.CountDownTimer;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.panama.Estructura.Funciones;
import com.example.panama.Estructura.Link;
import com.example.panama.Estructura.Variables;
import com.example.panama.R;
import com.example.panama.proyecto;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by reale on 13/07/2016.
 */
public class ViewPagerAdapter extends PagerAdapter {
    Activity activity;
    ArrayList<String> images = new ArrayList<String>();
    ArrayList<String> texto = new ArrayList<String>();
    ArrayList<String> titulo = new ArrayList<String>();
    ArrayList<String> sub = new ArrayList<String>();
    LayoutInflater inflater;
    Variables variables = new Variables();
    Funciones funciones = new Funciones();
    Link link = new Link();
    private Bitmap loadedImage;


    public ViewPagerAdapter(Activity activity, ArrayList images, ArrayList linkImagen, ArrayList titulo, ArrayList sub) {
        this.activity = activity;
        this.images = images;
        this.texto = linkImagen;
        this.titulo = titulo;
        this.sub = sub;

    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater)activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item,container,false);

        TextView lbl_titulo, lbl_sub;

        lbl_titulo = itemView.findViewById(R.id.lbl_titulo);
        lbl_sub = itemView.findViewById(R.id.lbl_sub);

        lbl_titulo.setText(titulo.get(position));

        lbl_sub.setText(sub.get(position));

        ImageView image;
        image = itemView.findViewById(R.id.imageView);
        /*DisplayMetrics dis = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dis);
        int height = dis.heightPixels;
        int width = dis.widthPixels;
        image.setMinimumHeight(height);
        image.setMinimumWidth(width);

        try{
            Picasso.with(activity.getApplicationContext())
                    .load(images.get(position))
                    .into(image);
        }
        catch (Exception ex){

        }*/

        String url = images.get(position);

        Glide.with(activity).load(images.get(position)).into(image);

//        ImageRequest imageRequest = new ImageRequest(url,
//                new Response.Listener<Bitmap>() {
//                    @Override
//                    public void onResponse(Bitmap response) {
//
//                    }
//
//
//                }, 0, 0,  null, new Response.ErrorListener(){
//
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        });



        image.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //variables.item_img = texto.get(position);

                if(variables.estado.equals("0")) {

                    variables.categoria = "4";

                    variables.item_id = variables.array_info_id.get(position);

                    variables.item_titulo = variables.array_info_titulo.get(position);

                    variables.item_genero = variables.array_info_sub.get(position);

                    funciones.JSONLOGIN(link.GET_Proyecto + variables.item_id);

                    new CountDownTimer(2000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            //resutlado.setText("seconds remaining: " + millisUntilFinished / 1000);
                        }

                        public void onFinish() {

                            if (variables.error.equals("1")) {

                                variables.estado = "1";

                                Intent intent = new Intent(activity, proyecto.class);

                                activity.startActivity(intent);

                                Log.d("JSON", "Carga Exitosa contenido");

                            } else {

                                Log.d("JSON", "Carga Fallida contenido");

                            }

                        }
                    }.start();

                }else{

                }



                /*Uri uri = Uri.parse(texto.get(position));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(intent);*/

            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View)object);
    }

    void downloadFile(String imageHttpAddress, ImageView imageView) {
        URL imageUrl = null;
        try {
            imageUrl = new URL(imageHttpAddress);
            HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
            conn.connect();
            loadedImage = BitmapFactory.decodeStream(conn.getInputStream());
            imageView.setImageBitmap(loadedImage);
        } catch (IOException e) {
            Toast.makeText(activity.getApplicationContext(), "Error cargando la imagen: "+e.getMessage(), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
