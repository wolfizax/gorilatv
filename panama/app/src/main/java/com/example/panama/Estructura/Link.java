package com.example.panama.Estructura;

public class Link {

    public static final String IP = "http://marcsof.com/public/project/api/";

    //GET --------------------------------

    public static String GET_Login = IP + "query_login.php?email=";

    public static String GET_Compras_ID = IP + "query_compras_id.php?id=";

    public static String GET_Proyectos = IP + "query_proyectos.php";

    public static String GET_Proyecto = IP + "query_proyecto.php?texto=";

    public static String GET_Galeria = IP + "query_galeria.php?texto=";

    public static String GET_Planos = IP + "query_planos.php?texto=";

    public static String GET_Categorias = IP + "query_all_categorias.php";

    public static String GET_Busqueda_id = IP + "query_search_id.php?busqueda=";

    public static String GET_Busqueda = IP + "query_search.php?busqueda=";

    public static String UPLOAD_URL = IP + "img/upload.php";

    //INSERT ------------------------------

    public static String INSERT_Usuarios = IP + "query_registrar.php";

    public static String INSERT_Compra = IP + "insert_compra.php";

    public static String INSERT_Paypal = IP + "insert_paypal.php";

}
