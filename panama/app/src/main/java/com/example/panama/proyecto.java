package com.example.panama;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.bumptech.glide.Glide;
import com.example.panama.Estructura.Funciones;
import com.example.panama.Estructura.Link;
import com.example.panama.Estructura.Objetos;
import com.example.panama.Estructura.Variables;

public class proyecto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyecto);

        OBJETOS();

        BOTONES();

        CARGAR();

        Lista();

        GALERIA();



    }

    Objetos objetos = new Objetos();

    Funciones funciones = new Funciones();

    Variables variables = new Variables();

    Link link = new Link();

    private void OBJETOS(){

        objetos.viewPager = findViewById(R.id.viewPager);

        objetos.lista = findViewById(R.id.lista);

        objetos.img_articulo = findViewById(R.id.img_titulo);

        objetos.lbl_contenido = findViewById(R.id.lbl_descripcion);

        objetos.c_galeria = findViewById(R.id.c_galeria);

        objetos.c_proyecto = findViewById(R.id.c_proyecto);

        objetos.img_planos = findViewById(R.id.img_planos);

        objetos.c_planos = findViewById(R.id.c_planos);

    }

    private void BOTONES(){



    }

    private void Lista() {

        objetos.lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if(position == 0){

                    funciones.go(proyecto.this, home.class);

                }else if(position == 1){

                    objetos.c_proyecto.setVisibility(View.VISIBLE);
                    objetos.c_galeria.setVisibility(View.GONE);
                    objetos.c_planos.setVisibility(View.GONE);

                }else if(position == 2){

                    objetos.c_galeria.setVisibility(View.VISIBLE);
                    objetos.c_proyecto.setVisibility(View.GONE);
                    objetos.c_planos.setVisibility(View.GONE);
                }else if(position == 3){

                    objetos.c_planos.setVisibility(View.VISIBLE);
                    objetos.c_galeria.setVisibility(View.GONE);
                    objetos.c_proyecto.setVisibility(View.GONE);

                }else if(position == 4){

                    Uri uri = Uri.parse(variables.item_youtube);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);

                }

            }
        });

    }

    private void CARGAR(){


        objetos.lbl_contenido.setText(variables.item_contenido);

        Log.d("JSON", variables.item_contenido);

        Glide.with(this).load(variables.item_img).into(objetos.img_articulo);



        variables.array_categorias_id.add("1");
        variables.array_categorias_id.add("2");
        variables.array_categorias_id.add("3");
        variables.array_categorias_id.add("4");
        variables.array_categorias_id.add("5");

        variables.array_categorias.add("VOLVER");
        variables.array_categorias.add("PROYECTO");
        variables.array_categorias.add("GALERIA");
        variables.array_categorias.add("PLANOS");
        variables.array_categorias.add("YOUTUBE");



        variables.array_categorias_img.add("http://art-mandisplay.com/IMG/flecha.png");
        variables.array_categorias_img.add("https://image.flaticon.com/icons/png/512/8/8811.png");
        variables.array_categorias_img.add("https://image.flaticon.com/icons/png/512/711/711810.png");
        variables.array_categorias_img.add("https://firebasestorage.googleapis.com/v0/b/urbania-app.appspot.com/o/acacias-garden-iii%2Fcarrusel.jpg?alt=media&token=2778331d-9149-4f98-8651-c2065fd58d3c");
        variables.array_categorias_img.add("https://image.flaticon.com/icons/png/512/9/9996.png");



        funciones.cargarLista(variables.array_categorias_id,  variables.array_categorias, variables.array_categorias_img, proyecto.this, objetos.lista);

        variables.array_categorias_id.clear();
        variables.array_categorias.clear();
        variables.array_categorias_img.clear();



    }

    private void GALERIA() {

        variables.categoria = "5";

        funciones.JSONLOGIN(link.GET_Galeria + variables.item_id);

        variables.categoria = "6";

        funciones.JSONLOGIN(link.GET_Planos + variables.item_id);

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
                //resutlado.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {

                if (variables.error.equals("1")) {

                    funciones.cargarPager(proyecto.this, objetos.viewPager, variables.array_info_galeria, variables.array_info_galeria_titulo, variables.array_info_galeria_sub);

                    Glide.with(proyecto.this).load(variables.item_planos).into(objetos.img_planos);

                    Log.d("JSON", "Carga Exitosa contenido");

                } else {

                    Log.d("JSON", "Carga Fallida contenido");

                }

            }
        }.start();

    }

    private void PAGER(){


        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/61/Landing%20ACACIAS-01.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/GARDEN%20I/Landing%20ACACIAS-05.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/GARDEN%20II/Landing%20ACACIAS-08.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/GARDEN%20III/Landing%20ACACIAS-12.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/LAS%20LAJAS/Landing%20ACACIAS-16.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/VERSALLES/Landing%20ACACIAS-20.jpg");

        variables.array_info_titulo.add("61");
        variables.array_info_titulo.add("Garden I");
        variables.array_info_titulo.add("Garden II");
        variables.array_info_titulo.add("Garden III");
        variables.array_info_titulo.add("Las Lajas");
        variables.array_info_titulo.add("Versalles");

        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");

        funciones.cargarPager(proyecto.this, objetos.viewPager, variables.array_info_img, variables.array_info_titulo, variables.array_info_sub);


    }

}
