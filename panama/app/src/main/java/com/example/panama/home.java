package com.example.panama;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.panama.Estructura.Funciones;
import com.example.panama.Estructura.Link;
import com.example.panama.Estructura.Objetos;
import com.example.panama.Estructura.Variables;

import java.util.TimerTask;

public class home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        OBJETOS();

        BOTONES();

        PROYECTOS();

        //PAGER();

        //Timer timer = new Timer();
        //timer.schedule(new MyTimerTask(), 3000, 5000);

    }

    Objetos objetos = new Objetos();

    Funciones funciones = new Funciones();

    Variables variables = new Variables();

    Link link = new Link();

    private void OBJETOS(){

        objetos.lbl_pais = findViewById(R.id.lbl_pais);

        objetos.lbl_inicio = findViewById(R.id.lbl_inicio);

        objetos.viewPager = findViewById(R.id.viewPager);

    }

    private void BOTONES(){

        objetos.lbl_pais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(home.this, Pais.class);

            }
        });

        objetos.lbl_inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(home.this, MainActivity.class);

            }
        });


    }

    private void PAGER(){

        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/61/Landing%20ACACIAS-01.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/GARDEN%20I/Landing%20ACACIAS-05.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/GARDEN%20II/Landing%20ACACIAS-08.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/GARDEN%20III/Landing%20ACACIAS-12.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/LAS%20LAJAS/Landing%20ACACIAS-16.jpg");
        variables.array_info_img.add("http://marcsof.com/public/project/ACACIAS/VERSALLES/Landing%20ACACIAS-20.jpg");

        variables.array_info_titulo.add("61");
        variables.array_info_titulo.add("Garden I");
        variables.array_info_titulo.add("Garden II");
        variables.array_info_titulo.add("Garden III");
        variables.array_info_titulo.add("Las Lajas");
        variables.array_info_titulo.add("Versalles");

        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");
        variables.array_info_sub.add("Acacias");



    }

    private void PROYECTOS() {

        variables.estado = "0";

        variables.categoria = "3";

        funciones.JSONLOGIN(link.GET_Proyectos);

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
                //resutlado.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {

                if (variables.error.equals("1")) {

                    funciones.cargarPager(home.this, objetos.viewPager, variables.array_info_img, variables.array_info_titulo, variables.array_info_sub);

                    Log.d("JSON", "Carga Exitosa contenido");

                } else {

                    Log.d("JSON", "Carga Fallida contenido");

                }

            }
        }.start();

    }

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            home.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(objetos.viewPager.getCurrentItem()==0){
                        objetos.viewPager.setCurrentItem(1);
                    }else if(objetos.viewPager.getCurrentItem()==1){
                        objetos.viewPager.setCurrentItem(2);
                    }else if(objetos.viewPager.getCurrentItem()==2){
                        objetos.viewPager.setCurrentItem(3);
                    }else if(objetos.viewPager.getCurrentItem()==3){
                        objetos.viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

}
