package com.example.panama;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.panama.Estructura.Funciones;
import com.example.panama.Estructura.Objetos;
import com.example.panama.Estructura.Variables;

public class nosotros extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nosotros);

        OBJETOS();

        BOTONES();

    }

    Objetos objetos = new Objetos();

    Funciones funciones = new Funciones();

    Variables variables = new Variables();

    private void OBJETOS(){

        objetos.lbl_regresar = findViewById(R.id.lbl_regresar);

        objetos.lbl_contacto = findViewById(R.id.lbl_contacto);


    }

    private void BOTONES(){

        objetos.lbl_contacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(nosotros.this, contacto.class);

            }
        });

        objetos.lbl_regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(nosotros.this, MainActivity.class);

            }
        });


    }

}
