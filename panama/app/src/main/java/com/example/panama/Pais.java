package com.example.panama;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.panama.Estructura.Funciones;
import com.example.panama.Estructura.Objetos;

public class Pais extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pais);

        OBJETOS();

        BOTONES();

    }

    Objetos objetos = new Objetos();

    Funciones funciones = new Funciones();

    private void OBJETOS(){

        objetos.lbl_inicio = findViewById(R.id.lbl_inicio);

        objetos.c_panama = findViewById(R.id.c_panama);

        objetos.c_espana = findViewById(R.id.c_espana);

    }

    private void BOTONES(){

        objetos.lbl_inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(Pais.this, MainActivity.class);

            }
        });

        objetos.c_panama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(Pais.this, home.class);

            }
        });

        objetos.c_espana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(Pais.this, home.class);

            }
        });

    }
}
