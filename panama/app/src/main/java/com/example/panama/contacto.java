package com.example.panama;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.panama.Estructura.Funciones;
import com.example.panama.Estructura.Objetos;
import com.example.panama.Estructura.Variables;

public class contacto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);
        OBJETOS();

        BOTONES();

    }

    Objetos objetos = new Objetos();

    Funciones funciones = new Funciones();

    Variables variables = new Variables();

    private void OBJETOS(){

        objetos.c_cerrar = findViewById(R.id.c_regresar);

        objetos.btn_enviar = findViewById(R.id.btn_enviar);


    }

    private void BOTONES(){

        objetos.c_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(contacto.this, MainActivity.class);

            }
        });

        objetos.btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.mensaje(contacto.this, "Funcion en Construccion");

            }
        });


    }
}
