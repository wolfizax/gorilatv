package com.example.panama;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.panama.Estructura.Funciones;
import com.example.panama.Estructura.Objetos;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        OBJETOS();

        BOTONES();

    }

    Objetos objetos = new Objetos();

    Funciones funciones = new Funciones();

    private void OBJETOS(){

        objetos.btn_enviar = findViewById(R.id.btn_proyectos);

        objetos.lbl_login = findViewById(R.id.lbl_login);

        objetos.lbl_pais = findViewById(R.id.lbl_pais);

        objetos.lbl_contacto = findViewById(R.id.lbl_contacto);

        objetos.lbl_nosotros = findViewById(R.id.lbl_nosotros);

    }

    private void BOTONES(){

        objetos.btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(MainActivity.this, home.class);

            }
        });

        objetos.lbl_pais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(MainActivity.this, Pais.class);

            }
        });

        objetos.lbl_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(MainActivity.this, login.class);

            }
        });

        objetos.lbl_nosotros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(MainActivity.this, nosotros.class);

            }
        });

        objetos.lbl_contacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(MainActivity.this, contacto.class);

            }
        });

    }


}
