package com.example.panama;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.panama.Estructura.Funciones;
import com.example.panama.Estructura.Objetos;

public class login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        OBJETOS();

        CARGAR();

    }

    Objetos objetos = new Objetos();

    Funciones funciones = new Funciones();

    private void OBJETOS(){

        objetos.web = findViewById(R.id.web);

    }

    private void CARGAR(){

        objetos.web.loadUrl("http://urbania.aal-team.com/login");

    }
}
