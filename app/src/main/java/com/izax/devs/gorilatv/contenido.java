package com.izax.devs.gorilatv;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.izax.devs.gorilatv.Estructura.Funciones;
import com.izax.devs.gorilatv.Estructura.Link;
import com.izax.devs.gorilatv.Estructura.Objetos;
import com.izax.devs.gorilatv.Estructura.Variables;

public class contenido extends AppCompatActivity {

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_contenido);

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3027457852301109~2484873634");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        OBJETOS();

        SELECCION();

    }

    Funciones funciones = new Funciones();

    Objetos objetos = new Objetos();

    Variables variables = new Variables();

    Link link = new Link();

    private void OBJETOS(){

        objetos.myrv = findViewById(R.id.recyclerview_id);

    }

    private void SELECCION(){

        if(variables.CATEGORIAS.equals("1")){

            categorias(link.GET_CategoriasLive);

        }

        if(variables.CATEGORIAS.equals("2")){

            categorias(link.GET_CategoriasSeries);

        }

        if(variables.CATEGORIAS.equals("3")){

            categorias(link.GET_CategoriasCine);

        }

        if(variables.CATEGORIAS.equals("4")){

            categorias(link.GET_Series_Select + variables.CATEGORIA_SERIES);

            Log.d("JSON", link.GET_Series_Select + variables.CATEGORIA_SERIES);

        }

        if(variables.CATEGORIAS.equals("5")){

            categorias(link.GET_Canales + variables.CATEGORIA_CANAL);

        }

    }

    private void categorias(String link){

        funciones.JSONLOGIN(link);

        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                //resutlado.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {

                if(variables.error.equals("1")){

                    funciones.cargarCard(contenido.this, objetos.myrv);

                    Log.d("JSON", "Carga Exitosa contenido");

                }else{

                    Log.d("JSON", "Carga Fallida contenido");

                }

            }
        }.start();

    }

    @Override
    public void onBackPressed(){

        funciones.go(contenido.this, MainActivity.class);

    }



}
