package com.izax.devs.gorilatv.Estructura;

import com.izax.devs.gorilatv.Items.Book;
import com.izax.devs.gorilatv.Items.Datos;
import com.izax.devs.gorilatv.Items.DatosArticulos;
import com.izax.devs.gorilatv.Items.Datos_EPG;

import java.util.ArrayList;
import java.util.List;

public class Variables {

    public static final int REQUEST_CALL = 1;

    public static final int idUnica = 51623;

    public static int PICK_IMAGE_REQUEST = 1;

    public static String KEY_IMAGEN = "foto";

    public static String KEY_NOMBRE = "id";

    public static Integer id_img = 0;

    public static String resultJSON;

    public static String
            id = "0", item_tipo = "", max_conect = "", nombre = "", apellido = "", email = "", password = "", conjunto = "", apto = "", interior = "", sexo = "", numero = "",
            vehiculo = "0", mascota = "0", opcion = "0", message = "", CATEGORIAS = "1", CATEGORIA_SERIES = "0", CATEGORIA_CINE = "0", CATEGORIA_CANAL = "0",
            categoria = "2", id_servicios = "0", item_comentario = "", idioma = "0", temporada = "1", img_serie = "",
            error = "0", permisos = "0", estado_search = "0", estado_menu = "0", consola = "0", item_id = "",
            id_item = "0", item_estado = "0", item_fondo = "", item_genero = "", item_titulo = "", item_titulo_ingles = " ", item_fecha = "", item_contenido = "", item_contenido_ingles = "", item_img = "", item_trailer = "", item_precio,
            day, month, year, hoy, item, telefono = "",
            mensaje_campos = "Debe llenar todos los campos.", mensaje = "", mensaje_fallido = "Error en el ingreso.", mensaje_exitoso = "Agregada exitosamente", mensaje_exitoso_actualizar = "Datos Actualizados."
            ;

    public static Double
            lat = 0.0, lon = 0.0;

    public static int
            dia = 0,mes = 0, ano = 0000, hora = 00, minutos = 00, tiempo = 5000 , valor_noticia = 0;

    public static List<Book> lstBook;

    //Array Datos Articulos ------------------------------------------------

    public static ArrayList<DatosArticulos> array_datos_articulos = new ArrayList<DatosArticulos>();

    public static ArrayList<Datos> array_datos_categorias = new ArrayList<Datos>();

    public static ArrayList<Datos_EPG> array_datos_epg = new ArrayList<Datos_EPG>();

    //Arrays Bitacora ------------------------------------------------------

    public static ArrayList<String> array_bitacora_id = new ArrayList<String>();

    public static ArrayList<String> array_bitacora_titulo = new ArrayList<String>();

    public static ArrayList<String> array_bitacora_contenido = new ArrayList<String>();

    public static ArrayList<String> array_bitacora_img = new ArrayList<String>();

    public static ArrayList<String> array_bitacora_fecha = new ArrayList<String>();

    //Arrays Notificaciones ---------------------------------------------

    public static ArrayList<String> array_notificaciones_id = new ArrayList<String>();

    public static ArrayList<String> array_notificaciones_mensaje = new ArrayList<String>();

    //Array Publicidad

    public static ArrayList<String> array_publicidad_id = new ArrayList<String>();

    public static ArrayList<String> array_publicidad_foto = new ArrayList<String>();

    //Array Categorias

    public static ArrayList<String> array_categorias_id = new ArrayList<String>();

    public static ArrayList<String> array_categorias = new ArrayList<String>();

    //Array Caratulas

    public static ArrayList<String> array_caratulas_id = new ArrayList<String>();

    public static ArrayList<String> array_caratulas_titulo = new ArrayList<String>();

    public static ArrayList<String> array_caratulas_foto = new ArrayList<String>();

    //Array Servicios

    public static ArrayList<String> array_servicios_id = new ArrayList<String>();

    public static ArrayList<String> array_servicios_foto = new ArrayList<String>();

    public static ArrayList<String> array_servicios_titulo = new ArrayList<String>();

    public static ArrayList<String> array_servicios_descripcion = new ArrayList<String>();

    public static ArrayList<String> array_servicios_titulo_ingles = new ArrayList<String>();

    public static ArrayList<String> array_servicios_descripcion_ingles = new ArrayList<String>();

    public static ArrayList<String> array_servicios_trailer = new ArrayList<String>();

    //Array Comentarios

    public static ArrayList<String> array_comentarios_id = new ArrayList<String>();

    public static ArrayList<String> array_comentarios_comentario = new ArrayList<String>();

    public static ArrayList<String> array_comentarios_fecha = new ArrayList<String>();

    //Array Series

    public static ArrayList<String> array_series_id = new ArrayList<String>();

    public static ArrayList<String> array_series_titulo = new ArrayList<String>();

    public static ArrayList<String> array_series_foto = new ArrayList<String>();

    //Array Temporadas

    public static ArrayList<String> array_temporadas_series_id = new ArrayList<String>();

    public static ArrayList<String> array_temporadas_series_titulo = new ArrayList<String>();

    public static ArrayList<String> array_temporadas_series_foto = new ArrayList<String>();

    //Array POST

    public static ArrayList<String> array_post = new ArrayList<String>();

    //Array EPG

    public static ArrayList<String> array_EPG = new ArrayList<String>();

    //Array Info

    public static ArrayList<String> array_info_img = new ArrayList<String>();

    public static ArrayList<String> array_info_descripcion = new ArrayList<String>();

    //Array HOME

    public static ArrayList<String> array_home_id = new ArrayList<String>();

    public static ArrayList<String> array_home_img = new ArrayList<String>();

    public static ArrayList<String> array_home_titulo = new ArrayList<String>();

}
