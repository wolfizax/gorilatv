package com.izax.devs.gorilatv.Items;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.izax.devs.gorilatv.Configuracion;
import com.izax.devs.gorilatv.Estructura.Funciones;
import com.izax.devs.gorilatv.Estructura.Variables;
import com.izax.devs.gorilatv.R;
import com.izax.devs.gorilatv.canales;
import com.izax.devs.gorilatv.contenido;

import java.util.ArrayList;

/**
 * Created by Aws on 28/01/2018.
 */

public class RecyclerViewAdapterHome extends RecyclerView.Adapter<RecyclerViewAdapterHome.MyViewHolder> {

    Variables variables = new Variables();
    Funciones funciones = new Funciones();

    int selectedPosition=-1;

    private Context mContext ;
    ArrayList<String> images = new ArrayList<String>();
    ArrayList<String> titulo = new ArrayList<String>();
    ArrayList<String> id = new ArrayList<String>();

    public RecyclerViewAdapterHome(Context mContext, ArrayList images, ArrayList titulo, ArrayList id) {
        this.mContext = mContext;
        this.images = images;
        this.titulo = titulo;
        this.id = id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardveiw_item_book2,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final Integer i = position;

        holder.tv_book_title.setText(titulo.get(position));

        Glide.with(mContext).load(images.get(position)).into(holder.img_book_thumbnail);

        if(selectedPosition==position)
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.tab_indicator_text));
        else
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //holder.linearLayout.setBackgroundColor(Color.RED);

                selectedPosition=position;
                notifyDataSetChanged();

                if(i == 0){

                    variables.array_publicidad_foto.clear();

                    variables.categoria = "3";

                    variables.CATEGORIAS = "1";

                    variables.item_tipo = "live";

                    Intent intent = new Intent(mContext, canales.class);

                    mContext.startActivity(intent);

                }else if(i == 1){

                    variables.array_publicidad_foto.clear();

                    variables.categoria = "3";

                    variables.CATEGORIAS = "3";

                    Intent intent = new Intent(mContext, contenido.class);

                    mContext.startActivity(intent);

                }else if(i == 2){

                    variables.array_publicidad_foto.clear();

                    variables.categoria = "3";

                    variables.CATEGORIAS = "2";

                    Intent intent = new Intent(mContext, contenido.class);

                    mContext.startActivity(intent);

                }else if(i == 3){

                    //funciones.mensaje(mContext, "Funcion en contruccion");

                }else if(i == 4){

                    //funciones.mensaje(MainActivity.this, "Funcion en contruccion");

                }else if(i == 5){

                    Intent intent = new Intent(mContext, Configuracion.class);

                    mContext.startActivity(intent);

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_book_title;
        ImageView img_book_thumbnail;
        CardView cardView;
        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_book_title = (TextView) itemView.findViewById(R.id.book_title_id) ;
            img_book_thumbnail = (ImageView) itemView.findViewById(R.id.book_img_id);
            cardView = (CardView) itemView.findViewById(R.id.cardview_id);
            linearLayout = itemView.findViewById(R.id.linear_fondo);

        }
    }

}
