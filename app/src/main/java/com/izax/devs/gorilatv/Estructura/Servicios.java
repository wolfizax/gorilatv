package com.izax.devs.gorilatv.Estructura;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Servicios {

    //GET -------------------------------------------

    public static class ObtenerRegistros extends AsyncTask<String,Void,String> {

        Variables variables = new Variables();

        JSONObject respuestaJSON;

        JSONObject respuestaJSON2;

        JSONObject respuestaJSON3;

        JSONObject respuestaJSON4;

        JSONArray contenidoJSON;

        Funciones funciones = new Funciones();

        private void Login() throws JSONException {

            variables.array_datos_articulos.clear();
            variables.array_bitacora_id.clear();
            variables.array_bitacora_titulo.clear();
            variables.array_bitacora_img.clear();
            variables.array_bitacora_contenido.clear();
            variables.array_bitacora_fecha.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {

                /*variables.id = contenidoJSON.getJSONObject(i).getString("id");
                variables.nombre = contenidoJSON.getJSONObject(i).getString("nombre");
                variables.apellido = contenidoJSON.getJSONObject(i).getString("apellido");
                variables.email = contenidoJSON.getJSONObject(i).getString("email");
                variables.password = contenidoJSON.getJSONObject(i).getString("password");*/

            }

        }

        private void Publicidad() throws JSONException {

            variables.array_categorias.clear();
            variables.array_categorias_id.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {

                variables.array_categorias_id.add(contenidoJSON.getJSONObject(i).getString("category_id"));

                Log.d("JSON ID CINE", contenidoJSON.getJSONObject(i).getString("category_id"));

                variables.array_categorias.add(contenidoJSON.getJSONObject(i).getString("category_name"));

            }

        }

        private void Servicios() throws JSONException {

            variables.array_servicios_id.clear();
            variables.array_servicios_foto.clear();
            variables.array_servicios_titulo.clear();
            variables.array_servicios_descripcion.clear();
            variables.array_servicios_titulo_ingles.clear();
            variables.array_servicios_descripcion_ingles.clear();
            variables.array_servicios_trailer.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {

                variables.array_servicios_id.add(contenidoJSON.getJSONObject(i).getString("id"));

                variables.array_servicios_foto.add(contenidoJSON.getJSONObject(i).getString("foto"));

                variables.array_servicios_titulo.add(contenidoJSON.getJSONObject(i).getString("titulo"));

                variables.array_servicios_descripcion.add(contenidoJSON.getJSONObject(i).getString("descripcion"));

                variables.array_servicios_titulo_ingles.add(contenidoJSON.getJSONObject(i).getString("titulo_ingles"));

                variables.array_servicios_descripcion_ingles.add(contenidoJSON.getJSONObject(i).getString("descripcion_ingles"));

                variables.array_servicios_trailer.add(contenidoJSON.getJSONObject(i).getString("trailer"));

                Log.d("VICENT", contenidoJSON.getJSONObject(i).getString("titulo"));

            }

        }

        private void Categorias() throws JSONException {

            variables.array_categorias_id.clear();
            variables.array_categorias.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {

                variables.array_categorias_id.add(contenidoJSON.getJSONObject(i).getString("id"));

                variables.array_categorias.add(contenidoJSON.getJSONObject(i).getString("categoria"));

            }

        }

        private void SERIES() throws JSONException {

            variables.array_series_id.clear();
            variables.array_series_titulo.clear();
            variables.array_series_foto.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {

                variables.array_series_id.add(contenidoJSON.getJSONObject(i).getString("series_id"));

                variables.array_series_titulo.add(contenidoJSON.getJSONObject(i).getString("name"));

                variables.array_series_foto.add(contenidoJSON.getJSONObject(i).getString("cover"));

            }

        }

        private void TEMPORADAS() throws JSONException {

            variables.array_categorias.clear();
            variables.array_categorias_id.clear();
            variables.array_publicidad_foto.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {

                variables.array_categorias_id.add(contenidoJSON.getJSONObject(i).getString("season_number"));

                variables.array_categorias.add(contenidoJSON.getJSONObject(i).getString("name"));

                variables.array_publicidad_foto.add(contenidoJSON.getJSONObject(i).getString("cover"));

                Log.d("JSON", contenidoJSON.getJSONObject(i).getString("name"));

            }

        }

        private void CANALES() throws JSONException {

            variables.array_categorias.clear();
            variables.array_categorias_id.clear();

            variables.array_categorias_id.add("0");
            variables.array_categorias.add("Atras");
            //int i = contenidoJSON.length(); i > 0; i--
            for (int i = 0; i < contenidoJSON.length(); i++) {

                variables.array_categorias_id.add(contenidoJSON.getJSONObject(i).getString("stream_id"));

                variables.item_id = contenidoJSON.getJSONObject(i).getString("stream_id");

                variables.array_categorias.add(contenidoJSON.getJSONObject(i).getString("name"));

                Log.d("JSON", contenidoJSON.getJSONObject(i).getString("name"));

            }



        }

        private void CAPITULOS() throws JSONException{

            variables.array_series_id.clear();
            variables.array_series_titulo.clear();
            variables.array_series_foto.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {

                variables.array_series_id.add(contenidoJSON.getJSONObject(i).getString("id"));

                variables.array_series_titulo.add(contenidoJSON.getJSONObject(i).getString("title"));

                Log.d("JSON TITULO", contenidoJSON.getJSONObject(i).getString("title"));

                variables.array_series_foto.add(variables.img_serie);

            }

        }

        private void PELICULAS() throws JSONException{

            variables.array_series_id.clear();
            variables.array_series_titulo.clear();
            variables.array_series_foto.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {

                variables.array_series_id.add(contenidoJSON.getJSONObject(i).getString("stream_id"));

                variables.array_series_titulo.add(contenidoJSON.getJSONObject(i).getString("name"));

                Log.d("JSON PELICULA", contenidoJSON.getJSONObject(i).getString("name"));

                variables.array_series_foto.add(contenidoJSON.getJSONObject(i).getString("stream_icon"));

            }

        }

        private void INFO (){

            variables.array_info_img.clear();
            variables.array_info_descripcion.clear();

            for (int i = 0; i < contenidoJSON.length(); i++) {


            }

        }

        private void HOME() throws JSONException {

            for (int i = 0; i < contenidoJSON.length(); i++) {

                Log.d("JSON EPG", contenidoJSON.getJSONObject(i).getString("start"));

            }

        }

        private void Marcador(String m) throws JSONException {

            if(m.equals("22")){

                Login();

            }else if(m.equals("3")){

                Publicidad();

            }else if(m.equals("4")){

                Servicios();

            }else if(m.equals("7")){

                SERIES();

            }else if(m.equals("6")){

                HOME();

            }else if(m.equals("10")){

                PELICULAS();

            }else if(m.equals("11")){

                CANALES();

            }

        }

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        //respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        if(params[2]=="2"){

                            //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                            respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                            variables.resultJSON = respuestaJSON.getString("user_info");//Estado es el nombre del campo en el JSON

                            Log.d("JSON", respuestaJSON.toString());

                            respuestaJSON2 = new JSONObject((variables.resultJSON));

                            Log.d("JSON", respuestaJSON2.toString());

                            variables.resultJSON = respuestaJSON2.getString("auth");

                            if (variables.resultJSON.equals("1")){ //hay alumnos a mostrar

                                variables.error = "1";

                                variables.message = respuestaJSON2.getString("message");

                                variables.nombre = respuestaJSON2.getString("username");

                                variables.estado_menu = respuestaJSON2.getString("status");

                                variables.max_conect = respuestaJSON2.getString("max_connections");

                            /*contenidoJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Marcador(params[2]);*/

                            }
                            else if (variables.resultJSON.equals("0")){

                                variables.error = "0";
                                Log.d("JSON", "Inicio Fallido");
                            }

                        }else{

                            if(params[2]=="8"){

                                respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                                variables.resultJSON = respuestaJSON.getString("seasons");//Estado es el nombre del campo en el JSON

                                contenidoJSON = new JSONArray(variables.resultJSON);

                                TEMPORADAS();

                            }else if(params[2]=="9") {

                                respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                                variables.resultJSON = respuestaJSON.getString("episodes");//Estado es el nombre del campo en el JSON

                                respuestaJSON2 = new JSONObject((variables.resultJSON));

                                variables.resultJSON = respuestaJSON2.getString(variables.temporada);

                                contenidoJSON = new JSONArray(variables.resultJSON);

                                Log.d("JSON PRUEBA", contenidoJSON.toString());

                                CAPITULOS();

                            }else if(params[2]=="6"){

                                respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                                variables.resultJSON = respuestaJSON.getString("epg_listings");//Estado es el nombre del campo en el JSON

                                contenidoJSON = new JSONArray(variables.resultJSON);

                                variables.array_EPG.clear();

                                variables.array_EPG.add("Ver canal");

                                for (int i = 0; i < contenidoJSON.length(); i++) {

                                    String coded = contenidoJSON.getJSONObject(i).getString("title");

                                    String inicia = contenidoJSON.getJSONObject(i).getString("start");

                                    String fin = contenidoJSON.getJSONObject(i).getString("end");

                                    String decodificar = new String(Base64.decode(coded, Base64.DEFAULT));

                                    Log.d("EPG", decodificar + " - " + inicia + " - " + fin);

                                    variables.array_EPG.add(decodificar + " - " + inicia + " - " + fin);

                                }

                            }else if(params[2]=="12"){

                                //contenidoJSON = new JSONArray(result.toString()); //Creo un JSONObject apartir de un JSONObject

                                //String van = contenidoJSON.getJSONObject(0).getString("info");

                                //String van_2 = contenidoJSON.getJSONObject(0).getString("movie_data");

                                Log.d("JSON PRUEBA", result.toString());

                                respuestaJSON = new JSONObject(result.toString());

                                variables.resultJSON = respuestaJSON.getString("info");

                                Log.d("JSON PRUEBA", variables.resultJSON);

                                respuestaJSON2 = new JSONObject(variables.resultJSON);

                                Log.d("JSON PRUEBA", respuestaJSON2.getString("backdrop_path"));

                                JSONArray data = respuestaJSON2.getJSONArray("backdrop_path");

                                Log.d("JSON PRUEBA", String.valueOf((data.get(0))));

                                variables.item_fondo = String.valueOf((data.get(1)));

                                variables.item_contenido = respuestaJSON2.getString("plot");

                                Log.d("JSON PRUEBA", variables.item_contenido);

                                variables.item_fecha = respuestaJSON2.getString("releasedate");

                                Log.d("JSON PRUEBA", variables.item_fecha);

                                variables.item_genero = respuestaJSON2.getString("genre");

                                Log.d("JSON PRUEBA", variables.item_genero);


                            }else{

                                    contenidoJSON = new JSONArray(result.toString());

                                    variables.error = "1";

                                    Marcador(params[2]);

                                    Log.d("JSON", "Consulta exitosa.");
                                }
                            }

                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                    Log.d("JSON", e.toString());
                }catch (IOException e) {
                    devuelve = e.toString();
                    Log.d("JSON", e.toString());
                } catch (JSONException e) {
                    devuelve = e.toString();
                    Log.d("JSON", e.toString());
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    //POST -----------------------------------------

    public static class Insert extends AsyncTask<String,Void,String> {

        Variables variables = new Variables();

        JSONObject jsonparam = new JSONObject();

        private void ENVIO() throws JSONException {

            if (variables.opcion.equals("0")){

                jsonparam.put("nombre", variables.nombre);
                jsonparam.put("email", variables.email);
                jsonparam.put("telefono", variables.telefono);
                jsonparam.put("sexo", variables.sexo);
                jsonparam.put("password", variables.password);
                jsonparam.put("conjunto", variables.conjunto);
                jsonparam.put("interior", variables.interior);
                jsonparam.put("numero", variables.numero);
                jsonparam.put("apto", variables.apto);
                jsonparam.put("vehiculo", variables.vehiculo);
                jsonparam.put("mascota", variables.mascota);

            }

        }

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="4"){  //update

                try {
                    HttpURLConnection urlConn;

                    DataOutputStream printout;
                    DataInputStream input;
                    url = new URL(cadena);
                    urlConn = (HttpURLConnection) url.openConnection();
                    urlConn.setDoInput(true);
                    urlConn.setDoOutput(true);
                    urlConn.setUseCaches(false);
                    urlConn.setRequestProperty("Content-Type", "application/json");
                    urlConn.setRequestProperty("Accept", "application/json");
                    urlConn.connect();
                    //creo el objeto json

                    ENVIO();

                    //envio los parametros post.
                    OutputStream os = urlConn.getOutputStream();
                    BufferedWriter write = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    write.write(jsonparam.toString());
                    write.flush();
                    write.close();

                    int respuesta = urlConn.getResponseCode();

                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){
                        String line;
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
                        while ((line = br.readLine()) != null){
                            result.append(line);
                        }

                        JSONObject respuestaJSON = new JSONObject(result.toString());

                        String resultJSON = respuestaJSON.getString("estado");

                        if (resultJSON == "1"){ //alumno actualizado correctamente
                            variables.error = "1";
                        }else if(resultJSON == "2"){
                            variables.error = "0";
                        }

                    }

                }catch (MalformedURLException e){
                    variables.error = e.toString();
                }catch (IOException e) {
                    variables.error = e.toString();
                } catch (JSONException e) {
                    variables.error = e.toString();
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {

            Log.d("LEANDRO", variables.error);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }


}
