package com.izax.devs.gorilatv;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.izax.devs.gorilatv.Estructura.Funciones;
import com.izax.devs.gorilatv.Estructura.Link;
import com.izax.devs.gorilatv.Estructura.Objetos;
import com.izax.devs.gorilatv.Estructura.Variables;

public class info extends AppCompatActivity {

    private AdView mAdView;

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_info);

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3027457852301109~2484873634");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        PUBLICIDAD();

        OBJETOS();

        BOTONES();

        JSON();

    }

    Funciones funciones = new Funciones();

    Objetos objetos = new Objetos();

    Variables variables = new Variables();

    Link link = new Link();

    private void PUBLICIDAD(){

        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {

                }
            }

            public void onFinish() {

                mInterstitialAd.show();

            }
        }.start();


    }

    private void OBJETOS(){

        objetos.lbl_titulo = findViewById(R.id.lbl_titulo);

        objetos.lbl_fecha = findViewById(R.id.lbl_fecha);

        objetos.lbl_contenido = findViewById(R.id.lbl_descripcion);

        objetos.lbl_categorias = findViewById(R.id.lbl_genero);

        objetos.img_articulo = findViewById(R.id.img);

        objetos.img_logo = findViewById(R.id.img_fondo);

        objetos.btn_enviar = findViewById(R.id.btn_reproducir);

    }

    private void BOTONES(){

        objetos.btn_enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    funciones.go(info.this, video.class);
                }

            }
        });

    }

    private void CONTENIDO(){

        Glide.with(this).load(variables.item_img).into(objetos.img_articulo);

        Glide.with(this).load(variables.item_fondo).into(objetos.img_logo);

        objetos.lbl_contenido.setText(variables.item_contenido);

        objetos.lbl_fecha.setText(variables.item_fecha);

        objetos.lbl_categorias.setText(variables.item_genero);

        objetos.lbl_titulo.setText(variables.item_titulo);



    }

    private void JSON(){

        variables.categoria = "12";

        funciones.JSONLOGIN(link.GET_Pelicula_Info + variables.item_id);

            new CountDownTimer(3000, 1000) {

                public void onTick(long millisUntilFinished) {
                    //resutlado.setText("seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {

                    CONTENIDO();

                }
            }.start();



    }


}
