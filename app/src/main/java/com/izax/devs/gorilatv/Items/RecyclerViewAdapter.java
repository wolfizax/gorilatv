package com.izax.devs.gorilatv.Items;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.izax.devs.gorilatv.Estructura.Funciones;
import com.izax.devs.gorilatv.Estructura.Variables;
import com.izax.devs.gorilatv.R;
import com.izax.devs.gorilatv.contenido;
import com.izax.devs.gorilatv.info;

import java.util.ArrayList;

/**
 * Created by Aws on 28/01/2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    Variables variables = new Variables();
    Funciones funciones = new Funciones();

    int selectedPosition=-1;

    private Context mContext ;
    ArrayList<String> images = new ArrayList<String>();
    ArrayList<String> titulo = new ArrayList<String>();
    ArrayList<String> id = new ArrayList<String>();

    public RecyclerViewAdapter(Context mContext, ArrayList images, ArrayList titulo, ArrayList id) {
        this.mContext = mContext;
        this.images = images;
        this.titulo = titulo;
        this.id = id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardveiw_item_book,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {



        holder.tv_book_title.setText(titulo.get(position));

        Glide.with(mContext).load(images.get(position)).into(holder.img_book_thumbnail);

        if(selectedPosition==position)
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.tab_indicator_text));
        else
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedPosition=position;
                notifyDataSetChanged();

                if(variables.categoria.equals("9") || variables.categoria.equals("10")) {

                    if(variables.categoria.equals("9")){

                        variables.item_tipo = "series";
                        variables.item_fondo = "";

                    }else{

                        variables.item_tipo = "movie";

                    }

                    Intent inten = new Intent(mContext, info.class);

                    mContext.startActivity(inten);

                    variables.item_img = images.get(position);
                    variables.item_titulo = titulo.get(position);
                    variables.item_id = id.get(position);

                }else{

                    variables.CATEGORIAS = "4";

                    variables.categoria = "8";

                    variables.CATEGORIA_SERIES = id.get(position);

                    variables.img_serie = images.get(position);

                    Intent intent = new Intent(mContext, contenido.class);

                    mContext.startActivity(intent);

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_book_title;
        ImageView img_book_thumbnail;
        CardView cardView ;

        public MyViewHolder(View itemView) {
            super(itemView);

            tv_book_title = (TextView) itemView.findViewById(R.id.book_title_id) ;
            img_book_thumbnail = (ImageView) itemView.findViewById(R.id.book_img_id);
            cardView = (CardView) itemView.findViewById(R.id.cardview_id);

        }
    }

}
