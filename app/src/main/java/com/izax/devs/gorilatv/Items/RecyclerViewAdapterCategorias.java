package com.izax.devs.gorilatv.Items;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.izax.devs.gorilatv.Estructura.Variables;
import com.izax.devs.gorilatv.R;
import com.izax.devs.gorilatv.caratulas;
import com.izax.devs.gorilatv.contenido;
import com.izax.devs.gorilatv.video;

import java.util.ArrayList;

/**
 * Created by Aws on 28/01/2018.
 */

public class RecyclerViewAdapterCategorias extends RecyclerView.Adapter<RecyclerViewAdapterCategorias.MyViewHolder> {

    Variables variables = new Variables();

    int selectedPosition=-1;

    private Context mContext ;
    ArrayList<String> images = new ArrayList<String>();
    ArrayList<String> titulo = new ArrayList<String>();
    ArrayList<String> id = new ArrayList<String>();

    public RecyclerViewAdapterCategorias(Context mContext, ArrayList images, ArrayList titulo, ArrayList id) {
        this.mContext = mContext;
        this.images = images;
        this.titulo = titulo;
        this.id = id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.cardveiw_item_book_categorias,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //--------------


        //--------------

        holder.tv_book_title.setText(titulo.get(position));

        Glide.with(mContext).load(images.get(position)).into(holder.img_book_thumbnail);

        if(selectedPosition==position)
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.holo_blue_light));
        else
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(android.R.color.background_light));

        

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectedPosition=position;
                notifyDataSetChanged();

                if(variables.CATEGORIAS.equals("1")){

                        variables.array_publicidad_foto.clear();

                        variables.categoria = "11";

                        variables.CATEGORIAS = "5";

                        variables.item_estado = "1";

                        variables.CATEGORIA_CANAL = id.get(position);

                        Intent intent = new Intent(mContext, contenido.class);

                        mContext.startActivity(intent);

                        Log.d("PELICULA", "Estas en ver categorias");

                }else if(variables.CATEGORIAS.equals("2")){

                    variables.CATEGORIA_SERIES = id.get(position);

                    Intent intent = new Intent(mContext, caratulas.class);

                    mContext.startActivity(intent);

                }else if(variables.CATEGORIAS.equals("4")){

                    variables.temporada = id.get(position);

                    variables.categoria = "9";

                    Log.d("JSON ESTOY", variables.categoria);

                    Intent intent = new Intent(mContext, caratulas.class);

                    mContext.startActivity(intent);

                }else if(variables.CATEGORIAS.equals("3")){

                    variables.CATEGORIA_CINE= id.get(position);

                    Intent intent = new Intent(mContext, caratulas.class);

                    mContext.startActivity(intent);

                }else if(variables.CATEGORIAS.equals("5")){

                    variables.item_id = id.get(position);

                    Intent intent = new Intent(mContext, video.class);

                    mContext.startActivity(intent);

                    Log.d("PELICULA", "Estas en ver Peliculas");

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_book_title;
        ImageView img_book_thumbnail;
        LinearLayout fondo;
        CardView cardView ;

        public MyViewHolder(View itemView) {
            super(itemView);

            fondo = itemView.findViewById(R.id.l_fondo);
            tv_book_title =  itemView.findViewById(R.id.book_title_id) ;
            img_book_thumbnail =  itemView.findViewById(R.id.book_img_id);
            cardView =  itemView.findViewById(R.id.cardview_id);

        }
    }

}
