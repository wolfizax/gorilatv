package com.izax.devs.gorilatv.Estructura;

import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.VideoView;

public class Objetos {

    public static EditText
            txt_email, txt_password, txt_password_rep, txt_search, txt_telefono, txt_edad, txt_nombre, txt_nueva, txt_actualizar, txt_sexo, txt_conjunto, txt_interior,
            txt_iglesia_nombre, txt_iglesia_direccion, txt_iglesia_email, txt_iglesia_telefono, txt_mensaje, txt_titulo, txt_contenido, txt_numero, txt_apto;

    public static TextView
            lbl_nombre, lbl_direccion, lbl_email, lbl_registrarse, lbl_olvido, lbl_telefono, lbl_iglesias, lbl_fecha, lbl_titulo,
            lbl_contenido, lbl_inicio, lbl_populares, lbl_categorias, lbl_español, lbl_ingles;

    public static ImageView
            img_perfil, img_salir, img_articulo, img_back, img_menu, img_search, img_logo, img_enviar, img_share,
            img_idioma, img_canales, img_series, img_peliculas, img_config;

    public static RecyclerView
            myrv;

    public static Button
            btn_login, btn_registrar, btn_llamar, btn_perfil, btn_regresar, btn_actualizar, btn_guardar, btn_calendario, btn_enviar,
            btn_aceptar, btn_cancelar, btn_opiniones, btn_opinar, btn_servicios, btn_preguntas, btn_preguntar, btn_trailer,
            btn_tv, btn_series, btn_cine, btn_token;

    public static FloatingActionButton
            fab;

    public static Switch sw_vehiculo, sw_mascota;

    public static ListView
            lista, lista_menu;

    public static ConstraintLayout
            layout_menu, c_login, c_cerrar, c_home, c_publicar, c_contratados, c_servicios, c_clientes, c_perfil, c_activo,
            c_one, c_xbox, c_ps3, c_ps4, c_psp, c_wii, c_u, c_pc;

    public static ViewPager
            viewPager;

    public static VideoView
            videoView;

    public static WebView
            web, web_clima;


}
