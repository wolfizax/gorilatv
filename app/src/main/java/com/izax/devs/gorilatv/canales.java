package com.izax.devs.gorilatv;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.izax.devs.gorilatv.Estructura.Funciones;
import com.izax.devs.gorilatv.Estructura.Link;
import com.izax.devs.gorilatv.Estructura.Objetos;
import com.izax.devs.gorilatv.Estructura.Variables;

public class canales extends AppCompatActivity {

    private AdView mAdView;

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_canales);

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3027457852301109~2484873634");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        PUBLICIDAD();

        OBJETOS();

        SELECCIONAR(0);

        LISTA();

    }

    Funciones funciones = new Funciones();

    Objetos objetos = new Objetos();

    Variables variables = new Variables();

    Link link = new Link();

    private void OBJETOS(){

        objetos.lista = findViewById(R.id.lista);

        objetos.lista_menu = findViewById(R.id.lista_epg);

        objetos.videoView = findViewById(R.id.videoView2);

    }

    private void PUBLICIDAD(){

        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {

                }
            }

            public void onFinish() {

                mInterstitialAd.show();

            }
        }.start();


    }

    private void LISTA() {

        objetos.lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {

                    objetos.lista_menu.setVisibility(View.VISIBLE);

                    objetos.videoView.setVisibility(View.VISIBLE);

                    if(variables.CATEGORIAS.equals("10")&&(position == 0)){

                        variables.categoria = "3";

                        variables.CATEGORIAS = "1";

                        variables.item_tipo = "live";

                        SELECCIONAR(position);

                    }else {

                        //MEDIMOS LA PANTALLA
                        Display display = getWindowManager().getDefaultDisplay();
                        int ancho = display.getWidth();
                        int  alto= display.getHeight();
                        //ASIGNAMOS MEDIANTE ID EL LAYOUT
                        //layout1 =(LinearLayout)findViewById(R.id.milayoutestirableporcodigo);
                        //POR EJEMPLO LE VOY A PONER 1/3, PARA QUE EN CUALQUIER
                        //DISPOSITIVO MIDA EL ALTO DE LA IMAGEN
                        //1/3 DE LA PANTALLA
                        objetos.lista.getLayoutParams().width = 800;
                        //ANCHO DE BANDA ARRIBA
                        //layout1.getLayoutParams().width=ancho;

                        SELECCIONAR(position);

                    }

                }



            }
        });

        objetos.lista_menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                funciones.go(canales.this, video.class);

            }
        });

    }

    private void SELECCIONAR(Integer i){

        if(variables.CATEGORIAS.equals("1")){

            CARGAR(link.GET_CategoriasLive);

            variables.categoria = "11";

            variables.CATEGORIAS = "5";

            variables.item_estado = "1";

            return;

        }

        if(variables.CATEGORIAS.equals("5")){

            variables.array_categorias.add("Atras");

            variables.CATEGORIA_CANAL = variables.array_categorias_id.get(i).toString();

            CARGAR(link.GET_Canales + variables.CATEGORIA_CANAL);

            Log.d("CANALES", link.GET_Canales + variables.CATEGORIA_CANAL);

            variables.CATEGORIAS = "10";

            variables.categoria = "6";

            return;

        }

        if(variables.CATEGORIAS.equals("10")){

            variables.CATEGORIA_CANAL = variables.array_categorias_id.get(i).toString();

            variables.item_id = variables.CATEGORIA_CANAL;

            Log.d("ID CANAL", link.GET_Canales_EPG + variables.CATEGORIA_CANAL);

            CARGAR_EPG(link.GET_Canales_EPG + variables.CATEGORIA_CANAL);

            REPRODUCTOR();

            return;

        }



    }

    private void CARGAR(String link){

        funciones.JSONLOGIN(link);

        new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {
                //resutlado.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {

                funciones.cargarLista(variables.array_categorias, variables.array_categorias, canales.this, objetos.lista);

            }
        }.start();

    }

    private void CARGAR_EPG(String link){

        funciones.JSONLOGIN(link);

        new CountDownTimer(1000, 1000) {

            public void onTick(long millisUntilFinished) {
                //resutlado.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {

                funciones.cargarListaEPG(variables.array_EPG, variables.array_EPG, canales.this, objetos.lista_menu);


            }
        }.start();

    }

    private void REPRODUCTOR(){

        String ruta2 = link.Peliculas +  variables.item_tipo +"/pruebaap/123456jjgg/" + variables.item_id + ".ts";

        Uri uri;

        uri  = Uri.parse(ruta2);

        //objetos.videoView.setMediaController(new MediaController(this));
        objetos.videoView.setVideoURI(uri);
        objetos.videoView.requestFocus();
        objetos.videoView.start();

    }




}
