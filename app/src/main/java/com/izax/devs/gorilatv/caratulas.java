package com.izax.devs.gorilatv;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.izax.devs.gorilatv.Estructura.Funciones;
import com.izax.devs.gorilatv.Estructura.Link;
import com.izax.devs.gorilatv.Estructura.Objetos;
import com.izax.devs.gorilatv.Estructura.Variables;

public class caratulas extends AppCompatActivity {

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_caratulas);

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3027457852301109~2484873634");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        OBJETOS();

        SELECCION();

    }

    Funciones funciones = new Funciones();

    Objetos objetos = new Objetos();

    Variables variables = new Variables();

    Link link = new Link();

    private void OBJETOS() {

        objetos.myrv = findViewById(R.id.recyclerview_id);

    }

    private void SELECCION(){

        if(variables.CATEGORIAS.equals("3")){

            variables.categoria = "10";

            categorias(link.GET_Peliculas + variables.CATEGORIA_CINE);

        }else{

            if (variables.categoria.equals("9")) {

                categorias(link.GET_Series_Select + variables.CATEGORIA_SERIES);

                Log.d("JSON", link.GET_Series_Select + variables.CATEGORIA_SERIES);

            } else {

                variables.categoria = "7";

                categorias(link.GET_Series + variables.CATEGORIA_SERIES);

            }

        }

    }

    private void categorias(String ruta) {

        funciones.JSONLOGIN(ruta);

        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                //resutlado.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {

                if (variables.error.equals("1")) {

                    funciones.cargarCararatulas(caratulas.this, objetos.myrv);

                    Log.d("JSON", "Carga Exitosa contenido");

                } else {

                    Log.d("JSON", "Carga Fallida contenido");

                }

            }
        }.start();

    }

    @Override
    public void onBackPressed() {

        funciones.go(caratulas.this, MainActivity.class);

    }

}