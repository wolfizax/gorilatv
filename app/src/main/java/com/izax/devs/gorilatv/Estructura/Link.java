package com.izax.devs.gorilatv.Estructura;

public class Link {

    public static final String IP = "http://iptv.gorilatv.com:25461/";

    //GET --------------------------------

    public static String GET_Login = "http://devsizax.com/proyectos/gorilatv/api/query_login.php?texto=";

    public static String GET_CategoriasLive = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_live_categories";

    public static String GET_CategoriasCine = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_vod_categories";

    public static String GET_CategoriasSeries = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_series_categories";

    public static String GET_Series = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_series&category_id=";

    public static String GET_Series_Select = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_series_info&series_id=";

    public static String GET_Peliculas = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_vod_streams&category_id=";

    public static String GET_Pelicula_Info = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_vod_info&vod_id=";

    public static String GET_Canales = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_live_streams&category_id=";

    public static String GET_Canales_EPG = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_short_epg&stream_id=";

    public static String GET_Canal_Home = IP + "player_api.php?username=pruebaapp&password=123456jjgg&action=get_live_streams&category_id=85";

    public static String Peliculas = "http://iptv.gorilatv.com:25461/";

}
