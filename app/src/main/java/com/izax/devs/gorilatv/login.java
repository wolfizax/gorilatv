package com.izax.devs.gorilatv;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.izax.devs.gorilatv.Estructura.Funciones;
import com.izax.devs.gorilatv.Estructura.Link;
import com.izax.devs.gorilatv.Estructura.Objetos;
import com.izax.devs.gorilatv.Estructura.Servicios;
import com.izax.devs.gorilatv.Estructura.Variables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class login extends AppCompatActivity {

    private InterstitialAd mInterstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3027457852301109~2484873634");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        PUBLICIDAD();

        OBJETOS();

        BOTONES();

    }

    Funciones funciones = new Funciones();

    Objetos objetos = new Objetos();

    Variables variables = new Variables();

    Link link = new Link();

    private void PUBLICIDAD(){

        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {

                }
            }

            public void onFinish() {

                mInterstitialAd.show();

            }
        }.start();


    }

    private void OBJETOS(){

        objetos.txt_email = findViewById(R.id.txt_usuario);

        objetos.txt_password = findViewById(R.id.txt_pass);

        objetos.btn_login = findViewById(R.id.btn_ingresar);

        objetos.btn_token = findViewById(R.id.btn_token);

    }

    private void BOTONES(){

        objetos.btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    LOGIN();
                }

            }
        });

        objetos.btn_token.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    funciones.go(login.this, Token.class);
                }
                //funciones.go(login.this, Token.class);

            }
        });

    }

    //Funcion Login - Acceso y validación -----------

    private void LOGIN(){

        variables.email = objetos.txt_email.getText().toString();

        JSONLOGIN(link.GET_Login + variables.email );


    }

    //----------------

    //GET -------------------------------------------

    ObtenerRegistros JSONRegistros;

    public void JSONLOGIN(String link){

        JSONRegistros = new ObtenerRegistros();
        JSONRegistros.execute(link ,"1");

    }

    public class ObtenerRegistros extends AsyncTask<String,Void,String> {

        Variables variables = new Variables();

        JSONObject respuestaJSON;

        JSONArray contenidoJSON;

    @Override
    protected String doInBackground(String... params) {

        String cadena = params[0];

        URL url = null; // url de donde queremos obtener la informacion

        String devuelve = "";

        if(params[1]=="1"){  //consulta por id

            try {
                url = new URL(cadena);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                        "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                int respuesta = connection.getResponseCode();
                StringBuilder result = new StringBuilder();

                if (respuesta == HttpURLConnection.HTTP_OK){

                    InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                    String line;
                    while ((line = reader.readLine()) != null){
                        result.append(line); //pasa toda la entrada al StringBuilder
                    }

                    //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                    respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                    variables.resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                    if (variables.resultJSON.equals("1")){ //hay alumnos a mostrar

                        variables.error = "1";

                        contenidoJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                    }
                    else if (variables.resultJSON.equals("2")){

                        variables.error = "0";
                    }

                }

            }catch (MalformedURLException e){
                devuelve = e.toString();
                Log.d("JSON", e.toString());
            }catch (IOException e) {
                devuelve = e.toString();
                Log.d("JSON", e.toString());
            } catch (JSONException e) {
                devuelve = e.toString();
                Log.d("JSON", e.toString());
            }

            return devuelve;

        }

        return null;
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
    }

    @Override
    protected void onPostExecute(String s) {

        if(variables.error.equals("1")){
            Intent intent = new Intent(login.this, MainActivity.class);
            startActivity(intent);
        }else{
            funciones.mensaje(login.this, "Codigo invalido.");
        }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

}

    //-----------
}
