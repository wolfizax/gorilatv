package com.izax.devs.gorilatv;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;

import com.izax.devs.gorilatv.Estructura.Funciones;
import com.izax.devs.gorilatv.Estructura.Link;
import com.izax.devs.gorilatv.Estructura.Objetos;
import com.izax.devs.gorilatv.Estructura.Variables;

public class video extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video);

        OBJETOS();

        BOTONES();

        REPRODUCTOR();

    }

    Funciones funciones = new Funciones();

    Objetos objetos = new Objetos();

    Variables variables = new Variables();

    Link link = new Link();

    private void OBJETOS() {

        objetos.videoView = findViewById(R.id.videoView);

        objetos.c_one = findViewById(R.id.c);

    }

    private void BOTONES() {

        objetos.c_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                funciones.go(video.this, MainActivity.class);

            }
        });

    }

    private void REPRODUCTOR(){

        String ruta = link.Peliculas +  variables.item_tipo +"/pruebaapp/123456jjgg/" + variables.item_id + ".mp4";

        Log.d("PELICULAS", ruta);

        String ruta2 = link.Peliculas +  variables.item_tipo +"/pruebaapp/123456jjgg/" + variables.item_id + ".ts";

        Uri uri;

        if(variables.CATEGORIAS.equals("10")){

           uri  = Uri.parse(ruta2);

            Log.d("PELICULA", ruta2);

        }else {

            uri = Uri.parse(ruta);

        }

        objetos.videoView.setMediaController(new MediaController(this));
        objetos.videoView.setVideoURI(uri);
        objetos.videoView.requestFocus();
        objetos.videoView.start();

    }

}
