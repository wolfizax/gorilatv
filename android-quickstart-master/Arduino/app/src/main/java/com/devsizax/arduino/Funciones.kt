package com.devsizax.arduino

import android.app.Activity
import android.content.Intent

class Funciones {

    fun go(activity: Activity, activity_go:Class<*>) {

        val intent = Intent(activity, activity_go)
        activity.startActivity(intent)
    }

}
