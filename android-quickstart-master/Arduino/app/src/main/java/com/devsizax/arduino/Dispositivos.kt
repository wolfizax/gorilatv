package com.devsizax.arduino

import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_dispositivos.*

abstract class Dispositivos : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispositivos)

        lista.adapter = MyCustomAdapter(this)


    }

    var TAG = "Dispositivos"

    var EXTRA_DEVICE_ADDRESS = "device_anddress"

    val mAdapter: BluetoothAdapter? = null

    private val mPairedDevicesArrayAdapter: ArrayAdapter<String>? = null

    private class MyCustomAdapter(context: Context): BaseAdapter(){

        private val mContext: Context

        init {
            mContext = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val textView = TextView(mContext)
            textView.text = "Lo que deseo ver"
            return textView
        }

        override fun getItem(position: Int): Any {
            return "Test String"
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {

            return  5
        }


    }

}
