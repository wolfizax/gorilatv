package com.devsizax.arduino

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var funciones = Funciones()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        BOTONES()


    }

    fun BOTONES(){

        btn_encender.setOnClickListener {

            funciones.go(this, Dispositivos::class.java)

        }

        btn_apagar.setOnClickListener {


        }

        btn_desconectar.setOnClickListener {


        }

    }
}

