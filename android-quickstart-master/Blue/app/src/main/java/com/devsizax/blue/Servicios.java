package com.devsizax.blue;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Servicios {

    //GET -------------------------------------------

    public static class ObtenerRegistros extends AsyncTask<String,Void,String> {

        Variables variables = new Variables();

        JSONObject respuestaJSON;

        JSONObject respuestaJSON2;

        JSONObject respuestaJSON3;

        JSONObject respuestaJSON4;

        JSONArray contenidoJSON;

        private void Login() throws JSONException {

                variables.id = contenidoJSON.getJSONObject(0).getString("id");
                variables.user = contenidoJSON.getJSONObject(0).getString("usuario");
                variables.tlf = contenidoJSON.getJSONObject(0).getString("tlf");

                Log.d("LEANDRO", variables.id + " " + variables.tlf);

        }



        private void Marcador(String m) throws JSONException {

            if(m.equals("2")){

                Login();

            }

        }

        @Override
        protected String doInBackground(String... params) {

            String cadena = params[0];

            URL url = null; // url de donde queremos obtener la informacion

            String devuelve = "";

            if(params[1]=="1"){  //consulta por id

                try {
                    url = new URL(cadena);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //abrir la coneccion
                    connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                            "(Linux; Android 1.5; es-ES) Ejemplo HTTP");

                    int respuesta = connection.getResponseCode();
                    StringBuilder result = new StringBuilder();

                    if (respuesta == HttpURLConnection.HTTP_OK){

                        InputStream in = new BufferedInputStream(connection.getInputStream());  //Preparo la cadena de entrada

                        BufferedReader reader = new BufferedReader(new InputStreamReader(in));  //Le introdusco en un BufferReader

                        String line;
                        while ((line = reader.readLine()) != null){
                            result.append(line); //pasa toda la entrada al StringBuilder
                        }

                        //creamos un objeto JSONObject para poder acceder a los atributos (campos) del objeto
                        respuestaJSON = new JSONObject(result.toString()); //Creo un JSONObject apartir de un JSONObject

                        variables.resultJSON = respuestaJSON.getString("estado");//Estado es el nombre del campo en el JSON

                        Log.d("JSON", variables.resultJSON.toString());

                        if (variables.resultJSON.equals("1")){ //hay alumnos a mostrar

                            variables.error = "1";

                            contenidoJSON = respuestaJSON.getJSONArray("oficinas"); //estado es el nombre del campo en el JSON

                            Marcador(params[2]);

                        }
                        else if (variables.resultJSON.equals("2")){

                            variables.error = "0";
                        }


                    }

                }catch (MalformedURLException e){
                    devuelve = e.toString();
                    Log.d("JSON", e.toString());
                }catch (IOException e) {
                    devuelve = e.toString();
                    Log.d("JSON", e.toString());
                } catch (JSONException e) {
                    devuelve = e.toString();
                    Log.d("JSON", e.toString());
                }

                return devuelve;

            }

            return null;
        }

        @Override
        protected void onCancelled(String s) {
            super.onCancelled(s);
        }

        @Override
        protected void onPostExecute(String s) {


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    }
